def calc(op,a,b):
    if op == "x":
        return a*b
    if op == "+":
        return a+b
    if op == "-":
        return a-b
    if op == "/":
        return a/b
    raise Exception


def handlecalc(args):
    [op, a, b] = args
    return calc(op, int(a), int(b))


if __name__ == "__main__":
    with open("step_4.txt", "r") as fname:
        commands = fname.readlines()
        done = False
        seen_lines = []
        next_line_read = 1
        command = commands[next_line_read - 1]
        while not done:
            seen_lines.append(command)
            tokens = command.split()

            match tokens:
                case ["goto", line_str]:
                    next_line_read = int(line_str)

                case ["goto", "calc", op, a, b]:
                    next_line_read = int(calc(op, int(a), int(b)))

                case ["remove", line_str]:
                    line = int(line_str)
                    if line <= len(commands):
                        del commands[line-1]
                    if line > prev_line_read:
                        next_line_read = prev_line_read + 1

                case ["replace", line_from_str, line_to_str]:
                    line_from = int(line_from_str)
                    line_to = int(line_to_str)
                    if line_to <= len(commands) and line_from <= len(commands):
                        commands[line_to-1] = commands[line_from-1]
                    next_line_read = prev_line_read + 1

            if next_line_read > len(commands):
                done = True
            else:
                command = commands[next_line_read-1]
                prev_line_read = next_line_read
                done = command in seen_lines
        print(prev_line_read, command)
